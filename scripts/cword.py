#!python

import sys
import os.path

currentdir = os.path.dirname(os.path.abspath(sys.argv[0]))
prefix = os.path.abspath(os.path.join(currentdir, '..'))
localmode = False
if 'setup.py' in os.listdir(prefix):
    if prefix not in sys.path:
        sys.path.insert(0, prefix)
    localmode = True
else:
    major, minor = sys.version_info[0], sys.version_info[1]
    dir = os.path.join(prefix, 'lib', 'python%d.%d' % (major, minor),
                       'site-packages')
    if dir not in sys.path:
        sys.path.append(dir)

print localmode
import cword.words
from cword.CWordHelper import CWordHelper

wordlist = cword.words.read_data ('w130794')
w = CWordHelper (wordlist)
import gtk
w.set_default_size (400, 500)
w.connect ('destroy', gtk.main_quit)
w.show_all ()
gtk.main()
