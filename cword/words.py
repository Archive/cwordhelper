#!/usr/bin/python
import cPickle
import os

class Wordlist:
    """Simple hash of words.  Able to do lookups and anagram searches.
    It's a bit heavyweight right now, so I may rewrite it in C in the
    future."""
    def __init__ (self):
        self.data = []
        self.max_word_len = 0
        self.skip_chars = '-_ '

    def _add_word (self, word, list):
        index_character = word[0]
        if len (word) > self.max_word_len:
            self.max_word_len = len(word)
        index = 0
        for (char, end_point, rest) in list:
            if char == index_character:
                if len (word) == 1:
                    list[index] = (index_character, True, rest)
                else:
                    self._add_word (word[1:], rest)
                return
            index += 1
        rest = []
        if len (word) == 1:
            list.append ((index_character, True, rest))
        else:
            list.append ((index_character, False, rest))
            self._add_word (word[1:], rest)

    def add_word (self, word):
        """Add a word to wordlist"""
        self._add_word (word, self.data)

    # Recursively search for a word.  called by self.search_for_word()
    def _search_for_word (self, target_word, trie_data, sub_word):
        end_reached = (len (target_word) == 1)
        index_character = target_word[0]
        results = []
        for (char, end_point, rest) in trie_data:
            if (index_character in self.skip_chars or
                index_character == char):
                # We have a potential match
                if end_reached and end_point:
                    results.append (sub_word + char)
                if not end_reached:
                    results += self._search_for_word (target_word[1:], rest, sub_word + char)
        return results
        
    def search_for_word (self, word):
        """Searches for word.  Word is composed of letter characters and '-' symbols"""
        results = self._search_for_word (word, self.data, '')
        if results:
            results.sort ()
        return results

    def _search_for_anagram (self, trie_data, letter_list, sub_word):
        end_reached = (len (letter_list) == 1)
        results = []
        for (char, end_point, rest) in trie_data:
            if char in letter_list:
                if end_point and end_reached:
                    results.append (sub_word + char)
                if not end_reached:
                    new_letter_list = list (letter_list)
                    del new_letter_list[new_letter_list.index (char)]
                    results += self._search_for_anagram (rest, new_letter_list, sub_word + char)
        return results

    def search_for_anagram (self, letters, word_list=[]):
        """Searches for an anagram made of letters"""
        letter_list = map (lambda x: x, letters)
        results = self._search_for_anagram (self.data, letter_list, '')
        if results:
            results.sort ()
        return results
        
def read_data(filename):
    try:
        os.stat (filename + ".pickle")
        f = open (filename + ".pickle", "rb")
        wordlist = cPickle.load (f)
        f.close()
    except OSError:
        i = 0
        wordlist = Wordlist ()
        f = file (filename)
        word = f.readline ()
        while word:
            if i == 1000000:
                break
            i += 1
            word = word.strip ().upper()
            wordlist.add_word (word)
            word = f.readline ()
        f.close ()
        try:
            f = open (filename + ".pickle", "wb")
            cPickle.dump (wordlist, f, True)
            f.close ()
        except:
            pass
    return wordlist

if __name__ == '__main__':
    import sys
    w = read_data ('w130794')
    print w.search_for_word (sys.argv[1])
    print w.search_for_anagram (sys.argv[1])
