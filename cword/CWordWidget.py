#!/usr/bin/python

import gobject
import pango
import gtk
import words

bad_hack = {}
for letter in 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789-_ ':
    bad_hack [ord(letter)] = letter.upper ()

class CWordWidget (gtk.DrawingArea):
    NORMAL=0
    SEARCHING=1
    __gsignals__ = {
        'lookup': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                   (str,)),
        'anagram': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                    (str,)),
        'changed': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE,
                    (str,))
    }

    def __init__ (self):
        gtk.DrawingArea.__init__ (self)
        self.__gobject_init__() # default constructor using our new GType
        self.set_flags (self.flags () | gtk.CAN_FOCUS)
        self.add_events (gtk.gdk.KEY_PRESS_MASK)
        self.connect ('size_request', self.do_size_request)
        self.connect ('expose_event', self.do_expose_event)
        self.connect ('key_press_event', self.do_key_press_event)
        self.connect ('focus_in_event', self.do_focus_in_event)
        self.connect ('focus_out_event', self.do_focus_out_event)

        ## FIXME: get a good size from elsewhere.
        self.max_characters = 15
        self.width = self.max_characters + 2
        self.height = 3
        self.square_size = 18
        self.square_border_width = 2
        self.text = ''
        self.clear_on_keypress = False
        self.cursor_timeout = -1
        self.cursor_toggle = True

    def update_cursor_toggle (self):
        self.cursor_toggle = not self.cursor_toggle
        self.queue_draw ()
        return True

    def setup_cursor_timeout (self):
        if self.cursor_timeout:
            gtk.timeout_remove (self.cursor_timeout)
        self.cursor_timeout = gtk.timeout_add (500, self.update_cursor_toggle)
        self.cursor_toggle = True

    def clear_text (self):
        self.clear_on_keypress = False
        self.text = ''
        self.emit ('changed', self.text)
        self.queue_draw ()
        
    def append_text (self, text):
        if not len (self.text) + len (text) > self.max_characters:
            self.text += text
            self.queue_draw ()
            self.emit ('changed', self.text)

    def do_focus_in_event (self, widget, event):
        self.setup_cursor_timeout ()
        return False

    def do_focus_out_event (self, widget, event):
        if self.cursor_timeout:
            gtk.timeout_remove (self.cursor_timeout)
        return False

    def do_anagram (self, text):
        self.clear_on_keypress = True

    def do_lookup (self, text):
        self.clear_on_keypress = True

    def do_changed(self, text):
        self.setup_cursor_timeout ()
        self.clear_on_keypress = False
        

    def do_key_press_event (self, widget, event):
        if event.keyval in (gtk.keysyms.Return, ):
            if self.text:
                if ' ' in self.text:
                    self.emit ('lookup', self.text)
                else:
                    self.emit ('anagram', self.text)
            return True
        elif event.keyval == gtk.keysyms.BackSpace:
            self.text = self.text[:-1]
            self.queue_draw ()
            self.emit ('changed', self.text)
            return True
        elif (event.keyval in (gtk.keysyms.L, gtk.keysyms.l) and \
              event.state & gtk.gdk.CONTROL_MASK) or \
              (event.keyval == gtk.keysyms.Escape) or \
              (event.keyval == gtk.keysyms.Delete):
            self.clear_text ()
            return True


        try:
            char = bad_hack[event.keyval]
        except KeyError:
            return False

        if self.clear_on_keypress:
            self.text = ''
            self.clear_on_keypress = False
        if char in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            self.append_text (char)
        if char in '123456789':
            count = int (char)
            self.append_text (' ' * count)
        if char in '-_ ':
            self.append_text (' ')
        return True
        
    def do_size_request (self, widget, requisition):
        requisition.width = self.width * self.square_size + \
                            (self.width + 1) * self.square_border_width
        
        requisition.height = self.height * self.square_size + \
                             (self.height + 1) * self.square_border_width
        return True

    def get_character_at_location (self, i, j):
        offset = int ((self.width - len (self.text))/2)
        if j != 1:
            return ''
        if i >= offset and i < offset + len (self.text):
            return self.text[i-offset]
        return ''

    def get_cursor_location (self):
        offset = int ((self.width - len (self.text))/2)
        if self.text:
            offset += len (self.text) - 1
        return (offset, 1)

    # Paint it every expose for simplicity
    def do_expose_event (self, widget, event):
        widget.window.draw_rectangle (widget.style.black_gc,
                                      True,
                                      0, 0,
                                      widget.allocation.width,
                                      widget.allocation.height)
        layout = pango.Layout (widget.get_pango_context())
        for i in range (0, self.width):
            for j in range (0, self.height):
                char = self.get_character_at_location (i, j)
                if char == '':
                    gc =widget.style.dark_gc[gtk.STATE_NORMAL]
                else:
                    gc = widget.style.white_gc
                    
                widget.window.draw_rectangle (gc,
                                              True,
                                              i * (self.square_size + self.square_border_width) + self.square_border_width,
                                              j * (self.square_size + self.square_border_width) + self.square_border_width,
                                              self.square_size,
                                              self.square_size)
                if char:
                    layout.set_text (char)
                    widget.window.draw_layout (widget.style.black_gc,
                                               int (i * (self.square_size + self.square_border_width) + self.square_border_width),
                                               int (j * (self.square_size + self.square_border_width) + self.square_border_width),
                                               layout)

        if self.cursor_toggle and gtk.HAS_FOCUS & self.flags():
            (cursor_x, cursor_y) = self.get_cursor_location ()
            widget.window.draw_rectangle (widget.style.white_gc,
                                          True,
                                          (cursor_x + 1) * (self.square_size + self.square_border_width),
                                          cursor_y * (self.square_size + self.square_border_width) + self.square_border_width,
                                          self.square_border_width,
                                          self.square_size)

        
        return True

        
gobject.type_register(CWordWidget)
                                
