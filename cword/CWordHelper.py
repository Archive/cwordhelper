#!/usr/bin/python

import gtk
import words
from CWordWidget import CWordWidget

class CWordHelper (gtk.Window):
    def __init__ (self, wordlist):
        gtk.Window.__init__ (self)
        self.wordlist = wordlist
        self.setup_widgets ()

    def setup_widgets (self):
        self.set_border_width (0)
        self.set_title ('Crossword Helper')
        vbox = gtk.VBox (False, 0)
        self.add (vbox)

        # Setup the CWordWidget
        align = gtk.Alignment (0.5, 0.5, 0.0, 0.0)
        align.set_border_width (24)
        frame = gtk.Frame (None)
        frame.set_shadow_type (gtk.SHADOW_ETCHED_IN)
        self.cword_widget = CWordWidget ()
        frame.add (self.cword_widget)
        align.add (frame)
        vbox.pack_start (align, False, False, 0)
        self.cword_widget.connect ('lookup', self.lookup_cb)
        self.cword_widget.connect ('anagram', self.anagram_cb)

        # Setup the result list
        sw = gtk.ScrolledWindow ()
        sw.set_shadow_type (gtk.SHADOW_IN)
        sw.set_policy (gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.result_view = gtk.TreeView()
        self.result_view.set_headers_visible (False)
        sw.add (self.result_view)
        vbox.pack_start (sw, True, True, 0)
        self.result_model = gtk.ListStore (str)
        self.result_view.set_model (self.result_model)
        column = gtk.TreeViewColumn (None, gtk.CellRendererText(), text=0)
        self.result_view.append_column (column)

        # Setup the statusbar
        self.status_bar = gtk.Statusbar ()
        self.status_bar.set_has_resize_grip (True)
        vbox.pack_start (self.status_bar, False, False, 0)


    def print_results (self, results):
        self.result_model.clear ()
        self.status_bar.pop (0)
        if results == []:
            self.status_bar.push (0, 'No matches found')
        elif len (results) == 1:
            self.status_bar.push (0, 'One match found')
        else:
            self.status_bar.push (0, '%d matches found' % len (results))

        for result in results:
            i = self.result_model.append ()
            self.result_model[i][0] = result

    def lookup_cb (self, widget, text):
        results = self.wordlist.search_for_word (text)
        self.print_results (results)
    
    def anagram_cb (self, widget, text):
        results = self.wordlist.search_for_anagram (text)
        self.print_results (results)
    
if __name__ == '__main__':
    wordlist = words.read_data ('w130794')
    w = CWordHelper (wordlist)
    w.set_default_size (400, 500)
    w.connect ('destroy', gtk.main_quit)
    w.show_all ()
    gtk.main()
