import sys, os
from distutils.sysconfig import get_python_lib

dest_dir = os.path.join(get_special_folder_path("CSIDL_COMMON_PROGRAMS"),
                        "CWordHelper")

if __name__ == "__main__":
    if "-install" == sys.argv[1]:
        try:
            os.mkdir(dest_dir)
        except OSError:
            pass # may already exist

        pythonw = os.path.join(sys.prefix, "pythonw.exe")
        create_shortcut(pythonw,
                        "Crossword Helper",
                        os.path.join(dest_dir, "CWordHelper.lnk"),
                        os.path.join(sys.prefix, "Scripts", "CWordHelper"))
        print 'Successfully made shortcut'
    elif "-remove" == sys.argv[1]:
        try:
            os.remove (os.path.join (dest_dir, "CWordHelper.lnk"))
            os.rmdir (dest_dir)
        except OSError:
            pass #may not have been created

        try:
            os.remove (os.path.join (sys.prefix, "CWordData", "w130794.pickle"))
        except OSError:
            pass #may not have been created



