#!/usr/bin/env python
from distutils.core import setup
import distutils.sysconfig
import sys
import os.path

try:
    target = sys.argv[1]
except IndexError:
    target = None

if target == 'bdist_wininst':
    datadir = 'CWordData'
else:
    prefix = distutils.sysconfig.get_config_vars ('prefix')
    prefix = prefix[0]
    datadir = os.path.join (prefix, 'share', 'CWordData')

setup (name='CWordHelper',
       version='0.1',
       packages=['cword'],
       data_files=[(datadir, ['cword/w130794']),],
       description='Simple crossword solver',
       long_description="""\
CWordHelper is a simple Crossword puzzle helper.  It will search for
words that are missing letters as well as solve anagrams.
""",
       author='Jonathan Blandford',
       author_email='jrb@alum.mit.edu',
       url='http://jrb.webwynk.net/',
       license='GPL',
       scripts=['scripts/CWordHelper', "cwordhelper-postinst-win32.py"],
       )
